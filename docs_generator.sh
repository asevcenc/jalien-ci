#!/bin/bash
export ROOT="$PWD"

#export PATH=/cvmfs/alice.cern.ch/bin:$PATH
#which alienv
#LATEST_ALIPHYSICS=$(alienv q | grep -i 'aliphysics.*root6' | grep -v 20210 | sort -g | tail -n 1)

#echo "$X509_CERT_DIR"

#alienv enter xjalienfs
export JALIEN_MAIN=${JALIEN_MAIN:-alien.JSh}
source <( /cvmfs/alice.cern.ch/bin/alienv printenv xjalienfs )
source <( alienv printenv JAliEn/latest-master-jalien )
which jalien
bash $ROOT/alienv_run.sh setup_ci_credentials

#echo "JALIEN"
#nohup jalien alien.JBox &
jalien -e commandlist &>/dev/null

#echo "ALIEN.PY"
#alien.py commandlist
#ls -l $ROOT/JAliEn
#export PATH=$ROOT/JAliEn:$PATH
#echo "$PATH"
#bash jalien ls
cd $ROOT/jalien-docs/
bash gen-check.sh

ls -larth

echo $?

