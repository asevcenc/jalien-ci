int tfile_test() {
  TGrid::Connect("alien://");
  const char *src = "alien:///alice/data/OCDBFoldervsRunRange.xml";
  const char *dst = "./OCDBFoldervsRange.xml";
  Bool_t retval = TFile::Cp(src, dst);
  return retval ? 0 : -1;
}
