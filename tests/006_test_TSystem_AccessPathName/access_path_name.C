using namespace std;

void die(const char *msg) {
  cout << "FAIL: " << msg << endl;
  exit(1);
}

void check(const char *path, bool target) {
  if (gSystem->AccessPathName(path) != target) {
    die(path);
  }
}

int access_path_name() {
  TGrid::Connect("alien");
  check("alien:///alice", false);
  check("alien:///doesnotexist", true);
  check("alien://doesnotexist", true);
  check("alien:///alice/data/OCDBFoldervsRunRange.xml", false);
  return 0;
}
